<?php namespace DeVosBurchart\Less;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot() {
		$this->package('bonroyage/less');

		if(!$this->app->runningInConsole()) {
			$smartless = new Less($this->app);
			$smartless->compile();
		}
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register() {
		$this->registerBindings();
		$this->registerCommands();
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides() {
		return array();
	}

	public function registerBindings() {
		$this->app['less'] = $this->app->share(function($app)
		{
			return new Less($app);
		});
	}

	public function registerCommands() {
		$this->app['command.less'] = $this->app->share(function($app)
		{
			return new Console\CompileCommand($app['less']);
		});

		$this->commands('command.less');
	}

}